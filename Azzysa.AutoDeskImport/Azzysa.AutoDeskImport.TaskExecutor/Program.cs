﻿namespace Azzysa.AutoDeskImport
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var endPoint = System.Configuration.ConfigurationManager.AppSettings["Azzysa.Import.Endpoint"];
                var connectionString = string.Empty;
                IAutoDeskConnector connector;

                // Get database type
                var databaseType = args[0];
                switch (databaseType.ToUpper())
                {
                    case "PSP":
                        System.Console.WriteLine("Database type: PSP");
                        connectionString = System.Configuration.ConfigurationManager.AppSettings["Azzysa.AutoDeskImport.TaskExecutor.PSP.ConnectionString"];
                        connector = new PSPImport.Library.PSPConnector();
                        break;
                    default:
                        throw new System.Exception("Invalid database type: " + databaseType);
                }

                // Set connection string to connector
                connector.ConnectionString = connectionString;

                // Get import command
                var importCmd = args[1];
                var importConnector = new ImportConnector();
                switch (importCmd.ToUpper())
                {
                    case "STANDARDPARTS":
                        System.Console.WriteLine("Import command: Standard-Parts");

                        // Get documents objects from connector and use import connector to convert to package and sent to back-end
                        importConnector.ImportStandardParts(endPoint, connector.GetStandardParts());

                        break;
                    default:
                        throw new System.Exception("Invalid command: " + importCmd);
                }

                System.Console.WriteLine("Finished");
            } catch (System.Exception ex)
            {
                System.Console.WriteLine(ex.ToString());
            }

            System.Console.ReadLine();
        }
    }
}