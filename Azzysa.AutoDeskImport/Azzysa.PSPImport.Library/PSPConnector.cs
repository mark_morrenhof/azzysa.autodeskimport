﻿using System.Collections.Generic;

namespace Azzysa.PSPImport.Library
{
    public class PSPConnector : AutoDeskImport.IAutoDeskConnector
    {
        /// <summary>
        /// Gets or sets the connection string to connect to the PSP database
        /// </summary>
        public string ConnectionString { get; set; }

        /// <summary>
        /// Returns a new <see cref="System.Data.OleDb.OleDbCommand"/> object to return all standard Pars 
        /// (associated <see cref="System.Data.OleDb.OleDbConnection"/> is closed and needs to be opened prior to execution)
        /// </summary>
        private System.Data.OleDb.OleDbCommand StandardPartsCommand {
            get {
                return new System.Data.OleDb.OleDbCommand(@"SELECT DOC.AIMKEY, DOC.IDENT, DOC.REVISION, DOC.FILE_NAME, DOC.FILE_EXT, DOC.FILE_UTC, DOC.FILE_TYPE, DOC.FILE_LINKNAME,
                    PRT.AIMKEY AS PART_AIMKEY, PRT.IDENT AS PART_NAME, PRT.REVISION AS PART_REVISION 
                FROM dbo.DOCUMENT DOC 
                    LEFT JOIN XREF_ELEMENT LNK ON LNK.PARENT_AIMKEY = DOC.AIMKEY AND LNK.RELATIONSHIP_ID = 'AIM.XREF.DOC.PART'
                    LEFT JOIN dbo.PART PRT on PRT.AIMKEY = LNK.CHILD_AIMKEY
                     WHERE DOC.FILE_TYPE = 'IPT' AND FILE_LINKNAME IS NOT NULL",
                new System.Data.OleDb.OleDbConnection(this.ConnectionString));
            }
        }
      
        /// <summary>
        /// Get all standard parts from the database
        /// </summary>
        /// <returns></returns>
        public List<AutoDeskImport.Document> GetStandardParts()
        {
            var partList = new List<AutoDeskImport.Document>();

            // Query all standard Parts
            var dbCommand = StandardPartsCommand;
            dbCommand.Connection.Open();
            try
            {
                var dbReader = dbCommand.ExecuteReader();
                while (dbReader.Read())
                {
                    partList.Add(new AutoDeskImport.Document()
                    {
                        Key = dbReader.GetDecimal(dbReader.GetOrdinal("AIMKEY")),
                        Name = dbReader.GetString(dbReader.GetOrdinal("IDENT")),
                        Revision = dbReader.GetString(dbReader.GetOrdinal("REVISION")),
                        FileName = dbReader.GetString(dbReader.GetOrdinal("FILE_NAME")),
                        FileExt = dbReader.GetString(dbReader.GetOrdinal("FILE_EXT")),
                        FileUtc = dbReader.GetDateTime(dbReader.GetOrdinal("FILE_UTC")),
                        FileType = dbReader.GetString(dbReader.GetOrdinal("FILE_TYPE")),
                        FileLinkName = dbReader.GetString(dbReader.GetOrdinal("FILE_LINKNAME")),
                        Part = new AutoDeskImport.Part()
                        {
                            Key = !dbReader.IsDBNull(dbReader.GetOrdinal("PART_AIMKEY")) ? dbReader.GetDecimal(dbReader.GetOrdinal("PART_AIMKEY")) : new decimal(),
                            Name = !dbReader.IsDBNull(dbReader.GetOrdinal("PART_NAME")) ? dbReader.GetString(dbReader.GetOrdinal("PART_NAME")) : string.Empty,
                            Revision = !dbReader.IsDBNull(dbReader.GetOrdinal("PART_REVISION")) ? dbReader.GetString(dbReader.GetOrdinal("PART_REVISION")) : string.Empty
                        }
                    });
                }
            } finally
            {
                dbCommand.Connection.Close();
            }

            // Finished
            return partList;
        }
    }
}