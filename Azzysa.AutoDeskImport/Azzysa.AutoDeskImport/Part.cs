﻿namespace Azzysa.AutoDeskImport
{
    public class Part : BaseObject
    {

        public override string ToString()
        {
            return string.Format("{0} {1}", new object[] { Name, Revision });
        }
    }
}
