﻿namespace Azzysa.AutoDeskImport
{
    public interface IAutoDeskConnector
    {
        /// <summary>
        /// Gets or sets the connection string to connect to the PSP database
        /// </summary>
        string ConnectionString { get; set; }

        /// <summary>
        /// Get all standard parts from the database
        /// </summary>
        /// <returns></returns>
        System.Collections.Generic.List<Document> GetStandardParts();
    }
}
