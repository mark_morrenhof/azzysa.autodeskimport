﻿namespace Azzysa.AutoDeskImport
{
    public class Document : BaseObject
    {
        public string FileName { get; set; }
        public string FileExt { get; set; }
        public System.DateTime FileUtc { get; set; }
        public string FileType { get; set; }
        public string FileLinkName { get; set; }
        public Part Part { get; set; }

        public override string ToString()
        {
            return string.Format("{0} {1} {2}", new object[] { Key.ToString(), Name, Revision });
        }

        public Infostrait.Azzysa.EF.Common.DataObject AsDataObject()
        {
            var dataObject = new Infostrait.Azzysa.EF.Common.DataObject();
            dataObject.Attributes.Add(new Infostrait.Azzysa.EF.Common.ObjectAttribute() { Name = "Key", Value = this.Key });
            dataObject.Attributes.Add(new Infostrait.Azzysa.EF.Common.ObjectAttribute() { Name = "Name", Value = this.Name });
            dataObject.Attributes.Add(new Infostrait.Azzysa.EF.Common.ObjectAttribute() { Name = "Revision", Value = this.Revision });
            dataObject.Attributes.Add(new Infostrait.Azzysa.EF.Common.ObjectAttribute() { Name = "FileName", Value = this.FileName });
            dataObject.Attributes.Add(new Infostrait.Azzysa.EF.Common.ObjectAttribute() { Name = "FileExt", Value = this.FileExt });
            dataObject.Attributes.Add(new Infostrait.Azzysa.EF.Common.ObjectAttribute() { Name = "FileUtc", Value = this.FileUtc });
            dataObject.Attributes.Add(new Infostrait.Azzysa.EF.Common.ObjectAttribute() { Name = "FileType", Value = this.FileType });
            dataObject.Attributes.Add(new Infostrait.Azzysa.EF.Common.ObjectAttribute() { Name = "FileLinkName", Value = this.FileLinkName });
            dataObject.Attributes.Add(new Infostrait.Azzysa.EF.Common.ObjectAttribute() { Name = "Part.Key", Value = this.Part?.Key });
            dataObject.Attributes.Add(new Infostrait.Azzysa.EF.Common.ObjectAttribute() { Name = "Part.Name", Value = this.Part?.Name });
            dataObject.Attributes.Add(new Infostrait.Azzysa.EF.Common.ObjectAttribute() { Name = "Part.Revision", Value = this.Part?.Revision });
            return dataObject;
        }
    }
}
