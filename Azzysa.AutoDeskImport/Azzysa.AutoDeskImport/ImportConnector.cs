﻿using System.Text;
using Newtonsoft.Json;
using System.Collections.Generic;
using System;

namespace Azzysa.AutoDeskImport
{
    public class ImportConnector
    {
        /// <summary>
        /// Initial batch size
        /// </summary>
        private static int batch_count = 25;

        /// <summary>
        /// Private helper method to create batch messages containing a datapackage with the configured amount of data packages as defined in <see cref="batch_count"/>
        /// </summary>
        /// <param name="url">Azzysa API endpoint URL where the package needs to be sent to</param>
        /// <param name="configurationName">Name of the azzysa.ef mapping that is set to the package object</param>
        /// <param name="execute">Delegate method that performs the actual work for example by running through a collection to add</param>
        /// <param name="batchSize">Overrided batch size (if applicable)</param>
        public static void Import(string url, string configurationName, Action<Action<Infostrait.Azzysa.EF.Common.DataObject>, Action> execute, int? batchSize = null)
        {
            var batchId = Guid.NewGuid();

            var dataPackage = new Infostrait.Azzysa.EF.Common.DataPackage();
            int processed = 0;
            int count = 0;

            #region Flush Items
            Action flush = () =>
            {
                // Sent package to AzzysaImport
                var batchMessage = new Infostrait.Azzysa.Batch.Kernel.BatchMessage();
                var converter = new JsonSerializer();
                var sb = new StringBuilder();
                var writer = new JsonTextWriter(new System.IO.StringWriter(sb));
                converter.Serialize(writer, dataPackage);
                batchMessage.Properties = new Dictionary<string, string>();
                batchMessage.Properties.Add("JsonPackage", sb.ToString());
                batchMessage.Properties.Add("ConfigurationName", configurationName);

                var json = JsonConvert.SerializeObject(batchMessage);
                var data = Encoding.Default.GetBytes(json);

                var request = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(url);
                request.Method = "POST";
                request.ContentType = "application/json";

                using (var rs = request.GetRequestStream())
                    rs.Write(data, 0, data.Length);

                using (var response = request.GetResponse())
                {
                    //do nothing with it
                }

                processed += dataPackage.DataObjects.Collection.Collection.Count;
                count++;

                dataPackage = new Infostrait.Azzysa.EF.Common.DataPackage();
            };
            #endregion


            execute((dataObject) =>
            {
                if (dataPackage.DataObjects.Collection.Collection.Count == (batchSize ?? batch_count))
                    flush();
                dataPackage.DataObjects.Collection.Collection.Add(dataObject);
                
            }, () =>
            {
                flush();
            });

            if (dataPackage.DataObjects.Collection.Collection.Count > 0)
                flush();
            
        }
        
        /// <summary>
        /// Import standard part documents into ENOVIA using the azzysa framework
        /// </summary>
        /// <param name="standardParts"></param>
        /// <param name="url"></param>
        public void ImportStandardParts(string url, List<Document> standardParts)
        {
            // Check for empty collection
            if (standardParts == null || standardParts?.Count == 0) return;

            // Convert to azzysa data packages
            ImportConnector.Import(url, "azzysa.ef.inventor.standardpart", (process, flush) =>
            {
                standardParts.ForEach(document => process(document.AsDataObject()));
            });
        }
    }
}