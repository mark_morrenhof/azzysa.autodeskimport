﻿namespace Azzysa.AutoDeskImport
{
    public class BaseObject
    {
        public decimal Key { get; set; }
        public string Name { get; set; }
        public string Revision { get; set; }
    }
}
